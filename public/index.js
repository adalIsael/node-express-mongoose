$(document).ready(function() {
    var getArticlesCall = {
        url: 'http://localhost:3000/articles',
        dataType: 'json',
        method: 'GET'
    };

    $.ajax(getArticlesCall)
        .done(printArticles)
        .fail(logearMensaje)
        .always(notificarUsuario)

    function printArticles(articles) {
        for (var i = 0; i < articles.length; i++) {
            var article = articles[i];
        var post = $('#article__empty')
            .clone()
            .removeAttr('id')
            .attr('id', article.id)

        $(post)
            .find('.article__content')
            .text(article.content)
            .end()
            .find('.article__title')
            .text(article.title)
            .end()
            .find('.article__author')
            .text(article.author)
            .end()
            .find('.article__link')
            .attr('href', article.link)
            .end()
            .appendTo('#articles .row');
        }
    }

    function logearMensaje(msg) {
        console.log(msg);
    }

    function notificarUsuario(msg) {
        console.log(msg);
    }
});
