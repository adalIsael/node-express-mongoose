var Article = require('../models/articles');

exports.readArticles = function(req, res){
  Article.find({})
  .then(function(articles){
    res.json(articles);
  })
  .catch(function(error){
    res.json(error);
  });
};

exports.readOneArticle = function(req, res){
  console.log('Reading article with ID' + req.params.id);
  Article.findById(req.params.id)
  .then(function(article){
    res.json(article);
  })
  .catch(function(error){
    console.error('Error updating article with ID' + req.params.id, error);
    res.json(error);
  })
};

exports.createArticle = function(req, res){
  var newArticle = new Article(req.body);
  newArticle.save()
  .then(function(article){
    res.json(article);
  })
  .catch(function(error){
    res.json(error)
  });
};

exports.updateOneArticle = function(req, res){
  var id = req.params.id;
  var options = {
    new: true
  };
  Article.findByIdAndUpdate(id, req.body, options)
  .then(function(article){
    res.json(article);
  })
  .catch(function(error){
    console.error('Error updating article with ID' + req.params.id, error);
    res.json(error);
  });
};

exports.deleteOneArticle = function(req, res){
  var id = req.params.id;
  Article.findByIdAndRemove(id)
  .then(function(){
    res.json({});
  })
  .catch(function(error){
    console.error('Error deleting article with ID' + req.params.id, error);
    res.json(error);
  });
};
